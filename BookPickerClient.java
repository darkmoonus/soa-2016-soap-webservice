package SOAP;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class BookPickerClient {

    public static void main(String[] args) throws Exception {
        URL url = new URL("http://localhost:9999/bookPicker?wsdl");
        QName qname = new QName("http://SOAP/", "BookPickerImplService");
        Service service = Service.create(url, qname);
        BookPicker b = service.getPort(BookPicker.class);

        System.out.println(b.getBookPrice("5") == -1 ? "Not exist" : b.getBookPrice("5"));
        System.out.println(b.getBookPrice("2") == -1 ? "Not exist" : b.getBookPrice("2"));
        System.out.println(b.getBookPrice("1") == -1 ? "Not exist" : b.getBookPrice("1"));
    }
}
package SOAP;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.util.HashMap;
import java.util.Map;

@WebService(endpointInterface = "SOAP.BookPicker")
public class BookPickerImpl implements BookPicker {

    public static Map<String, Double> bookPriceMap = new HashMap<>();
    static {
        bookPriceMap.put("1", 100.1);
        bookPriceMap.put("2", 200.2);
        bookPriceMap.put("3", 300.3);
        bookPriceMap.put("4", 400.4);
    }

    @Override
    public double getBookPrice(String id) {
        Double price = bookPriceMap.get(id);
        return price == null ? -1 : price;
    }

    public static void main(String[] args) {
        System.out.println("Server running...");
        Endpoint.publish("http://localhost:9999/bookPicker", new BookPickerImpl());
    }
}
